<?php

/** @noinspection PhpUnused */

use Tetrode\MyDBR\Extensions\WebService\WebService;

require_once 'vendor/autoload.php';

function Ext_WebservicePre($id, $options, $colInfo)
{
    WebService::init($id, $options, $colInfo);
    if (WebService::isErrorFound())
    {
        $error = WebService::getErrorMessage();
        // TODO handle this by propagating this to MyDBR
        /** @noinspection PhpUndefinedFunctionInspection */
        error_print("Error: {$error}");
        /** @noinspection PhpUndefinedFunctionInspection */
        error_print("Usage: select 'dbr.webservice', 'URL");
    }
}

function Ext_WebserviceRow($dataRow)
{
    Webservice::addRow($dataRow);
}

function Ext_WebserviceAfter()
{
    return Webservice::execute();
}

function Ext_Webservice_Passthrough($id, $options, $dataIn, $colInfo)
{
    WebService::init($id, $options, $colInfo);
    foreach ($dataIn as $item)
    {
        WebService::addRow($item);
    }
    list($columns, $data) = WebService::execute();
    /** @noinspection PhpUndefinedClassInspection */
    Extension::result_set($data, $columns);
}
