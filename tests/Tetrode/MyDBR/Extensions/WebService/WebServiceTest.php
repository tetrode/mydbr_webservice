<?php
    /**
     * @noinspection PhpComposerExtensionStubsInspection
     *               gives issues on vm42
     */

    namespace Tetrode\MyDBR\Extensions\WebService;

    use PHPUnit\Framework\TestCase;

    class WebServiceTest
        extends TestCase
    {
        public function testWebService()
        {
            $options = [
                'dbr.webservice'        => ['url' => 'https://dev.smartcall.cc/api/session/api/configuration/TEST'],
                'dbr.webservice.debug'  => ['debug' => '2'],
                'dbr.webservice.header' => [
                    'name'  => [
                        'Content-Type',
                        'Language',
                    ],
                    'value' => [
                        'application/json',
                        'nl-be',
                    ],
                ],
                'dbr.webservice.query'  => [
                    'name'  => 'a',
                    'value' => 'b',
                ],
                'dbr.webservice.type'   => ['type' => 'POST'],
                'dbr.webservice.return' => ['fields' => 'Body, Headers, NOProtocolVersion, Reason Phrase, Status Code'],
            ];

            $columnInformation['datatype'] = [
                'char',
                'char',
            ];
            $columnInformation['name'] = [
                'col1',
                'col2',
            ];
            /** @noinspection SpellCheckingInspection */
            $columnInformation['columnreference'] = [
                'col1',
                'col2',
            ];

            WebService::init(1, $options, $columnInformation);
            WebService::addRow([
                'r1c1',
                'r1c2',
            ]);
            WebService::addRow([
                'r2c1',
                'r2c2',
            ]);
            //list($type, $parameters, $body) = WebService::execute();
            list($columns, $data) = WebService::execute();

            $this->assertEquals("the URL", $parameters['base_uri']);
            $this->assertEquals("b", $parameters['query']['a']);
            $this->assertEquals("application/json", $parameters['headers']['Content-Type']);
            $this->assertEquals("nl-be", $parameters['headers']['Language']);
            $this->assertEquals("GET", $type);
            $body = json_decode($body);
            $this->assertEquals("r1c2", $body->r1c1);
            $this->assertEquals("r2c2", $body->r2c1);
        }
    }
