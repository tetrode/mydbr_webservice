<?php

namespace Tetrode\MyDBR\Extensions\WebService;

use PHPUnit\Framework\TestCase;
use UnexpectedValueException;

class OptionsTest extends TestCase
{
    public function testOptionsFull()
    {
        $opt = [
            'dbr.webservice'        => ['url' => 'URL'],
            'dbr.webservice.debug'  => ['debug' => '2'],
            'dbr.webservice.header' => [
                'name'  => ['Content-Type', 'Language'],
                'value' => ['application/json', 'nl-be',],
            ],
            'dbr.webservice.query'  => ['name' => 'a', 'value' => 'b'],
            'dbr.webservice.type'   => ['type'=>'GET'],
            'dbr.webservice.return' => ['fields' => 'Body, Headers, Protocol Version, Reason Phrase, Status Code'],
        ];

        $options = new Options($opt);
        $this->assertEquals('URL', $options->url);
        $this->assertEquals('2', $options->debug);
        $this->assertEquals("application/json", $options->headers['Content-Type']);
        $this->assertEquals("nl-be", $options->headers['Language']);
        $this->assertEquals("b", $options->query['a']);
        $this->assertEquals('GET', $options->type);
        $this->assertEquals('Body, Headers, Protocol Version, Reason Phrase, Status Code', $options->return['fields']);
    }

    public function testOptionsMinimal()
    {
        $opt = [
            'dbr.webservice'        => ['url' => 'URL'],
        ];

        $options = new Options($opt);
        $this->assertEquals('URL', $options->url);
        $this->assertEquals('0', $options->debug);
        $this->assertEquals([], $options->headers);
        $this->assertEquals([], $options->query);
        $this->assertEquals('GET', $options->type);
    }

    /**
     *
     */
    public function testOptionsNoURL()
    {
        $opt = [
            'dbr.webservice'        => ['foo' => 'bar'],
        ];

        $this->expectException(UnexpectedValueException::class);
        new Options($opt);
    }
}
