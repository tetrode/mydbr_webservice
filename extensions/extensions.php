<?php

$dbr_extensions['webservice'] = ([
    'enabled'          => true,
    'autoload'         => 0,
    'name'             => 'Web Services',
    'php'              => 'webservice.php',
// Define these if you want to do row by row handling (and set single_pass_call =>'')
// 'row_by_row_initialization' => 'Ext_WebservicePre',
// 'row_by_row_data_row' => 'Ext_WebserviceRow',
// 'row_by_row_finish' => 'Ext_WebserviceAfter',
// Define this if you want to handle data in one pass (Ext_WebserviceAll) (and set previous three =>'')
    'single_pass_call' => 'Ext_Webservice_Passthrough',
    'javascript'       => [],
    'css'              => [],
//'mydbrextension' => 1,
    'passthrough'      => true,
    'cmds'             => [
        [
            'cmd' => 'dbr.webservice',
            'url' => 1,
        ],
        [
            'cmd'   => 'dbr.webservice.header',
            'name'  => 1,
            'value' => 1,
        ],
        [
            'cmd'   => 'dbr.webservice.query',
            'name'  => 1,
            'value' => 1,
        ],
        [
            'cmd'  => 'dbr.webservice.type',
            'type' => 1,
        ],
        [
            'cmd'    => 'dbr.webservice.return',
            'fields' => 1,
        ],
        [
            'cmd'   => 'dbr.webservice.debug',
            'debug' => 1,
        ],
    ],
]);
