<?php


    namespace Tetrode\MyDBR\Extensions\WebService;

    use UnexpectedValueException;

    class Options
    {
        public $url;
        public $debug;
        public $headers;
        public $query;
        public $type;
        public $return;

        /**
         * Options constructor.
         *
         * @param $options
         *
         * @throws UnexpectedValueException
         */
        public function __construct($options)
        {
            $this->url = isset($options['dbr.webservice']['url']) ? $options['dbr.webservice']['url'] : '';
            if ($this->url == '') {
                throw new UnexpectedValueException("Parameter dbr.webservice.url is required");
            }
            $this->debug = isset($options['dbr.webservice.debug']['debug']) ? $options['dbr.webservice.debug']['debug'] : 0;
            $this->type = isset($options['dbr.webservice.type']['type']) ? $options['dbr.webservice.type']['type'] : 'GET';
            $this->headers = isset($options['dbr.webservice.header']) ? $this->convert($options['dbr.webservice.header']) : [];
            $this->query = isset($options['dbr.webservice.query']) ? $this->convert($options['dbr.webservice.query']) : [];
            $this->return = isset($options['dbr.webservice.return']) ? $options['dbr.webservice.return'] : '';
        }

        private function convert($opt)
        {
            $result = [];
            if (is_array($opt['name'])) {
                for ($i = 0; $i < count($opt['name']); $i++) {
                    $result[$opt['name'][$i]] = $opt['value'][$i];
                }
            } else {
                $result[$opt['name']] = $opt['value'];
            }

            return $result;
        }
    }
