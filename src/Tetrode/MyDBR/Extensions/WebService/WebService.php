<?php /** @noinspection PhpComposerExtensionStubsInspection */

namespace Tetrode\MyDBR\Extensions\WebService;

use Exception;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use stdClass;

class WebService
{
    /** @var bool When more columns are present, an automatic conversion will be done to JSON */
    private static $conversion;
    /** @var array */
    private static $columnInformation;
    /** @var array */
    private static $rows;

    /** @var number UNUSED */
    private static $id;
    /** @var string */
    private static $errorMessage = "";
    /** @var bool */
    private static $errorFound = false;
    /** @var \Tetrode\MyDBR\Extensions\WebService\Options */
    private static $opt;

    /**
     * addRow is called every time a row is encountered
     *
     * @param $row
     */
    public static function addRow($row)
    {
        self::$rows[] = $row;
    }

    /**
     * execute is called at the end
     *
     * @return array
     */
    public static function execute()
    {
        $body = self::convert();
        $clientConfig = [
            'base_uri' => self::$opt->url,
            'query'    => self::$opt->query,
            'headers'  => self::$opt->headers,
            'body'     => $body,
        ];
        self::outputDebugInformation('Webservice parameters', $clientConfig, 1);
        self::outputDebugInformation('Webservice body', $body, 1);

        $client = new Client($clientConfig);
        $response = $client->request(self::$opt->type, self::$opt->url);

        return [
            self::createColumns(),
            self::createData($response),
        ];
    }

    /**
     * create the web service body
     *
     * @return string
     */
    private static function convert()
    {
        if (self::$conversion)
        {
            $a = new stdClass();
            foreach (self::$rows as $row)
            {
                $a->{$row[0]} = $row[1];
            }
            $body = json_encode($a);
        }
        else
        {
            $body = self::$rows[0][0];
        }

        return $body;
    }

    /**
     * Output debug information: title and optional a variable
     *
     * @param $title string
     * @param $var   string
     */
    private static function outputDebugInformation($title, $var = null, $level = 1)
    {
        if (self::$opt->debug >= $level)
        {
            echo sprintf("<div style='text-align:left;'><h2>%s</h2>", $title);
            if (!is_null($var))
            {
                dump($var);
            }
            echo '</div>';
        }
    }

    private static function createColumns()
    {
        $fields = array_map('trim', explode(',', self::$opt->return['fields']));
        $columns = [
            ['name' => 'Body', 'datatype' => 'char'],
            ['name' => 'Headers', 'datatype' => 'char'],
            ['name' => 'Protocol Version', 'datatype' => 'char'],
            ['name' => 'Reason Phrase', 'datatype' => 'char'],
            ['name' => 'Status Code', 'datatype' => 'int'],
        ];

        $result = [];
        foreach ($fields as $field)
        {
            foreach ($columns as $column)
            {
                if ($field == $column['name'])
                {
                    $result[] = $column;
                }
            }
        }

        return $result;
    }

    private static function createData(ResponseInterface $response)
    {
        $fields = array_map('trim', explode(',', self::$opt->return['fields']));
        $data = [
            'Body'             => $response->getBody()
                ->getContents(),
            'Headers'          => json_encode($response->getHeaders()),
            'Protocol Version' => $response->getProtocolVersion(),
            'Reason Phrase'    => $response->getReasonPhrase(),
            'Status Code'      => $response->getStatusCode(),
        ];

        $result = array_filter(
            $data,
            function ($key) use ($fields)
            {
                return is_numeric(array_search($key, $fields));
            },
            ARRAY_FILTER_USE_KEY
        );
        return [array_values($result)];
    }

    public static function init($id, $options, $columnInformation)
    {
        self::$id = $id;
        self::$errorFound = false;
        self::$errorMessage = "";

        try
        {
            self::$opt = new Options($options);
        }
        catch (Exception $e)
        {
            self::$errorMessage = $e->getMessage();
            self::$errorFound = true;

            return;
        }

        self::$columnInformation = $columnInformation;
        self::$conversion = (count($columnInformation['datatype']) !== 1);
        self::$rows = [];
        /*
         * select a,b,c from table
         * a    b   c
         * -------------
         * x    y   z
         * q    s   d
         * w    x   c
         * -------------
         * {
         *      "x": "y",
         *      "q": "s",
         *      "w": "x"
         * }
         */

        self::outputDebugInformation(
            sprintf("%s<br>Result Set ID: {self::$id}", __METHOD__),
            [
                "url"                => self::$opt->url,
                "type"               => self::$opt->type,
                "headers"            => self::$opt->headers,
                "query"              => self::$opt->query,
                "column information" => self::$columnInformation,
                "conversion"         => self::$conversion,
            ],
            2
        );
    }

    /**
     * @return string
     */
    public static function getErrorMessage()
    {
        return self::$errorMessage;
    }

    /**
     * @return bool
     */
    public static function isErrorFound()
    {
        return self::$errorFound;
    }
}
